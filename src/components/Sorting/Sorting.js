class Sorting {
    static init() {
        $('.Sorting').each((i, item) => {
            new Sorting($(item));
        });
    }

    constructor($instance) {
        this.$instance = $instance;
        this.$instance.data('Sorting', this);
        this.$select = $instance.find('.Sorting__Select');

        this.paramsString = location.search;
        this.searchParams = new URLSearchParams(this.paramsString);

        this.handle();
    }

    handle() {
        this.$select.on('change', (e) => {
            this.handleOnChange(e);
            for (let p of this.searchParams) {
                console.log(p);
              }
        })
    }

    handleOnChange(e) {
        e.preventDefault();


        var sortValues = this.$select.val();
        if(this.$select.val()=="") {
            this.searchParams.delete("select");
            window.location.href = location.pathname + '?' + this.searchParams;
        } else {
            this.searchParams.set("select", sortValues);
            window.location.href = location.pathname + '?' + this.searchParams;
        }
    }
}
export default Sorting;