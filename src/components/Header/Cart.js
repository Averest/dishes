class Cart {
    static init() {
        $('.Header__cart').each((i, item) => {
            new Cart($(item));
        });
    }

    constructor($instance) {
        this.$instance = $instance;
        this.$instance.data('Cart', this);
        this.$CartCount = $instance.find(".Header__cartBuy");
        this.cartnumber = 0;
        
        this.handle();
    }

    handle() {
        console.log(this.cartnumber);
    }

    addToCart( count ) {
        this.cartnumber += count;
        this.updateCart();
    }

    addToTwoCart( count ) {
        this.cartnumber += count;
        this.updateCart();
    }

    removeFromCart( count ) {
        this.cartnumber -= count;
        this.updateCart();
    }

    updateCart() {
        this.$CartCount.text(this.cartnumber);
    }
}

export default Cart;