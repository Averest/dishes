class Input {
    static init() {
        $('.Form__Input').each((i, item) => {
            new Input($(item));
        });
    }

    constructor($instance) {
        this.$instance = $instance;
        this.$instance.data('Input', this);
        this.$input = $instance;
        this.isValid = false;

        this.handle();
    }

    handle() {
        this.$input.on('input', (e) => { 
            this.handleInput(e); })
    }

    handleInput(e) {
        this.isValid = this.validate();

        if(this.$instance.length) {
            this.$instance.closest('form').data('Form').handleInputs();
        }
    }

    validate() {
        if(this.$input.val()) {
            return true;
        }
        return false;
    }
}

export default Input;