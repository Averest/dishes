// include jQuery with standart object
global.$ = require('jquery.min.js');
global.jQuery = $;
require('jquery-ui.js');
require('jquery-ui.combobox');
import Global from 'Global/Global.js';
import Input from 'Form/Input.js';
import Textarea from 'Form/Textarea.js';
import Form from 'Form/Form.js';
import Header from 'Header/Header.js';
import ContentMenu from 'ContentMenu/ContentMenu.js';
import Menu from 'Header/Menu.js';
import Cart from 'Header/Cart.js';
import SoupCards from 'SoupCards/SoupCards.js';
import Sorting from 'Sorting/Sorting.js';


$(document).on('mobileinit', function() {
    $.mobile.loadingMessage = false;
    $.mobile.loader.prototype.options.disabled = true;
    $.mobile.loading( "hide" );
    $.mobile.loading().hide();
    $.mobile.ajaxEnabled=false;
    $.mobile.autoInitializePage=false;
    $.mobile.linkBindingEnabled=false;
    $.mobile.hashListeningEnabled=false;
});

$(document).ready(function() {
    $(".ui-loader").remove();

    Global.init();
    Header.init();
    Menu.init();
    Sorting.init();
    ContentMenu.init();
    Input.init();
    Textarea.init();
    Cart.init();
    SoupCards.init();
    Form.init();
});
