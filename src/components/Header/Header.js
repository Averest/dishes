class Header {
    static init() {
        $('.Header__menu').each((i, item) => {
            new Header($(item));
        });
    }

    constructor($instance) {
        this.$instance = $instance;
        this.$instance.data('Header', this);
        this.$button = $instance.find("[type=button]");
        this.$menu = $instance.find(".Burger");

        this.handle();
    }

    handle() {
        this.$button.on('click', (e) => {
            e.stopPropagation();
            this.handleOnClick(e)
        });
        this.$menu.on('click', (e) => {
            e.stopPropagation();
        });
        this.closeMenu();
        $(window).on('click', (e) => {
            this.closeMenu();
        })
    }


    handleOnClick(e) {
        e.preventDefault();
        if (this.$menu.hasClass("Burger__shown")) {
            this.closeMenu();
        } else {
            this.openMenu();
        }
        
    }

    openMenu() {
        this.$menu.addClass("Burger__shown");
    }

    closeMenu() {
        this.$menu.removeClass("Burger__shown");
    }
}

export default Header;