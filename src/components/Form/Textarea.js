class Textarea {
    static init() {
        $('.Textarea').each((i, item) => {
            new Textarea($(item));
        });
    }

    constructor($instance) {
        this.$instance = $instance;
        this.$instance.data('Textarea', this);
        this.$textarea = $instance;
        this.isValid = false;

        this.handle();
    }

    handle() {        
        this.$textarea.on('input', (e) => {this.handleTextarea(e); })
    }

    handleTextarea(e) {
        this.isValid = this.validate();
        if(this.$instance.length) {
                this.$instance.closest('form').data('Form').handleInputs();
            }
    }

    validate() {
        if(this.$textarea.val()) {
            return true;
        }
        return false;
    }
}

export default Textarea;