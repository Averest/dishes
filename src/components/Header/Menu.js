class Menu {
    static init() {
        $('.Header__menuItem').each((i, item) => {
            new Menu($(item));
        });
    }
    constructor($instance) {
        this.$instance = $instance;
        this.$instance.data('Menu', this)

        this.handle();
    }

    handle() {
        this.$instance.on('click', (e) => {
            this.handleOnClick(e);
        });
    }

    handleOnClick(e) {
        if (this.$instance.hasClass("Header__MenuItemChecked")) {
            this.handleClose();
        } else {
            this.handleOpen();
        }
    } 

    handleOpen() {
        this.$instance.addClass("Header__MenuItemChecked");
    }

    handleClose() {
        this.$instance.removeClass("Header__MenuItemChecked")
    }
}
export default Menu;