class Form {
    static init() {
        $('form').each((i, item) => {
            new Form($(item));
        });
    }

    constructor($instance) {
        this.$instance = $instance;
        this.$instance.data('Form', this);
        this.$comment = $instance.find('[name=comment]');
        this.$email = $instance.find('[type=email]');
        this.$submit = $instance.find('[type=submit]');

        this.handle();
    }
    handle() {
        this.$instance.on('submit', (e) => {this.handleSubmit(e)});
        this.handleInputs();
    }

    handleSubmit(e){
        e.preventDefault();
        let form_data = new FormData();
        form_data.append('action', 'form_request');
        form_data.append('comment', this.$comment.val());
        form_data.append('email', this.$email.val());
	$.post({
            data: form_data,
            url: testAjaxObject['ajaxurl'],
            contentType: false,
            processData: false,
            timeout: 9000, 
            success: (data) => {
                console.log("success", data);
                data = JSON.parse(data);
                if(data && data.status === 'success') {
                    this.handleSuccess();
                } else {
                    console.log("success-error", data);
                }   
            },  
            error: (data) => {
                console.log("error", data);
		this.handleError();
            }   
        });
    }

    handleSuccess() {
        alert("Форма отправлена");
    }
    handleError() {
        alert("Ошибка!");
    }

    handleInputs() {
        if(
            (typeof this.$email.data('Input') !== 'undefined') &&
            (typeof this.$comment.data('Textarea') !== 'undefined') &&
            this.$email.data('Input').isValid &&
            this.$comment.data('Textarea').isValid
        ) {
            this.$submit.removeAttr('disabled');
        } else {
            this.$submit.attr('disabled', true);
        }
    }
}

export default Form;