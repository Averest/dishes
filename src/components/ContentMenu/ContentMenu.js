class ContentMenu {
    static init() {
        $('.ContentMenu__Link').each((i, item) => {
            new ContentMenu($(item));
        });
    }
    constructor($instance) {
        this.$instance = $instance;
        this.$instance.data('ContentMenu', this);

        this.handle();
    }

    handle() {
        this.$instance.on('click', (e) => {
            this.handleOnClick(e);
        });
    }

    handleOnClick(e) {
        if (this.$instance.hasClass("ContentMenu__LinkChecked")) {
            this.handleClose();
        } else {
            this.handleOpen();
        }
    } 

    handleOpen() {
        this.$instance.addClass("ContentMenu__LinkChecked");
    }

    handleClose() {
        this.$instance.removeClass("ContentMenu__LinkChecked")
    }
}
export default ContentMenu;