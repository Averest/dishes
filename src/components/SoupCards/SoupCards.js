import { conditionalExpression } from "babel-types";

class SoupCards {
    static init() {
        $('.SoupCards__Frame').each((i, item) => {
            new SoupCards($(item));
        });
    }

    constructor($instance) {
        this.$instance = $instance;
        this.$instance.data('SoupCards', this);
        this.Cart = $('.Header__cart').data('Cart');
        this.$button = $instance.find(".SoupCards__Price");
        this.$remove = $instance.find(".remove");
        this.$count = $instance.find(".count");
        this.$CCount = $instance.find(this.$CartCount);
        this.$animated = $instance.find(".SoupCards__Animated");
        this.$saleprice = $instance.find(".SoupCards__SalePrice");
        this.number = 0;
        this.count = 0;
        
        this.handle();
    }

    handle() {
        this.$button.on('click', (e) => {
            this.handleAddCount(e);  
        });
        this.$saleprice.on('click', (e) => {
            this.handleAddTwoCount(e);  
        });
        this.$remove.on('click', (e) => {
            this.handleRemoveCount(e);
        });
    }

    handleAddCount(e) {
        e.preventDefault();
        this.openNapkin();
        this.number++;
        this.Cart.addToCart(1);
        this.$count.text(this.number);
    }

    handleAddTwoCount(e) {
        e.preventDefault();
        this.openNapkin();
        this.number +=1;
        this.Cart.addToTwoCart(1);
        this.$count.text(this.number);
    }

    handleRemoveCount(e) {
        this.closeNapkin();
        this.Cart.removeFromCart(this.number);
        this.$count.text('');
        this.number= 0;     
    }

    openNapkin() {
        this.$animated.addClass("Shown");
    }

    closeNapkin() {
        this.$animated.removeClass("Shown");
    }
}

export default SoupCards;